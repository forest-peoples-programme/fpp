<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d6\Node;

/**
 * Drupal 6 node source from database.
 */
class FppNode extends Node {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // For some reason this query doesn't switch = IN based on an array.
    if (isset($this->configuration['node_types'])) {
      $query->condition('n.type', $this->configuration['node_types'], 'IN');
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $return = parent::prepareRow($row);
    $nid = $row->getSourceProperty('nid');
    $vid = $row->getSourceProperty('vid');

    // Include path alias.
    $query = $this->select('url_alias', 'ua')
      ->fields('ua', ['dst']);
    $query->condition('ua.src', 'node/' . $nid);
    $alias = $query->execute()->fetchField();
    if (!empty($alias)) {
      $row->setSourceProperty('alias', '/' . $alias);
    }

    // Move image references to image field.
    $images = [];
    $related = [];
    foreach ((array) $row->getSourceProperty('field_related_nodes') as $item) {
      $query = $this->select('node', 'n');
      $query->condition('nid', $item['nid']);
      $query->fields('n', ['type']);
      $type = $query->execute()->fetchField();
      if ($type == 'image') {
        $images[] = $item;
      }
      else {
        $related[] = $item;
      }
    }
    $row->setSourceProperty('images', $images);
    $row->setSourceProperty('related', $related);

    // Retrieve terms.
    // select tn.tid, td.vid from term_node tn inner join term_data td on
    // tn.tid = td.tid where tn.nid = 6250 and tn.vid = 20436;
    $query = $this->select('term_node', 'tn');
    $query->join('term_data', 'td', 'tn.tid = td.tid');
    $query->condition('tn.nid', $nid);
    $query->condition('tn.vid', $vid);
    $query->fields('td', ['tid', 'vid']);
    $terms = $query->execute()->fetchAll();

    $topics = $regions = $partners = $principles = $tags = $newsletter = $blogs = [];
    $lookup = [
      1 => 'topics',
      2 => 'regions',
      11 => 'partners',
      12 => 'principles',
      13 => 'tags',
      15 => 'newsletter',
      16 => 'blogs',
    ];
    foreach ($terms as $term) {
      if (isset($lookup[$term['vid']])) {
        ${$lookup[$term['vid']]}[] = $term;
      }
    }
    $row->setSourceProperty('terms_topics', $topics);
    $row->setSourceProperty('terms_regions', $regions);
    $row->setSourceProperty('terms_partners', $partners);
    $row->setSourceProperty('terms_principles', $principles);
    $row->setSourceProperty('terms_tags', $tags);
    $row->setSourceProperty('terms_newsletter', $newsletter);
    $row->setSourceProperty('terms_blogs', $blogs);

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
