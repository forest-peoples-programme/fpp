<?php

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Taxonomy term i18n_strings translations from database.
 *
 * @MigrateSource(
 *   id = "fpp_term_i18n_strings",
 *   source_provider = "taxonomy"
 * )
 */
class TermI18nStrings extends DrupalSqlBase {

  /**
   * Name of the term data table.
   *
   * @var string
   */
  protected $termDataTable;

  /**
   * Name of the term hierarchy table.
   *
   * @var string
   */
  protected $termHierarchyTable;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('i18n_strings', 'i');
    $query->join('term_data', 'td', 'i.objectid = td.tid');
    $query->join('locales_target', 't', 't.lid = i.lid');
    $query->condition('td.vid', $this->configuration['vocabulary'], 'IN')
      ->condition('i.property', 'name')
      ->isNotNull('t.language')
      ->fields('td', ['tid', 'vid'])
      ->orderBy('td.tid');
    $query->addField('t', 'language');
    $query->addField('t', 'translation', 'name');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = array(
      'tid' => $this->t('The term ID.'),
      'vid' => $this->t('Existing term VID'),
      'name' => $this->t('The translation of the name.'),
      'description' => $this->t('The translation of the description.'),
      'language' => $this->t('The language code of the translation'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Add description if there is one, doing this in one SQL query was too much
    // for my SQLfoo.
    $query = $this->select('term_data', 'td');
    $query->join('i18n_strings', 'i', 'i.objectid = td.tid');
    $query->join('locales_target', 't', 't.lid = i.lid');
    $query->condition('td.tid', $row->getSourceProperty('tid'))
      ->condition('i.property', 'description')
      ->condition('t.language', $row->getSourceProperty('language'))
      ->addField('t', 'translation', 'description');
    $description = $query->execute()->fetchField();
    $row->setSourceProperty('description', $description);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['tid']['type'] = 'integer';
    $ids['language']['type'] = 'string';
    $ids['language']['alias'] = 't';
    return $ids;
  }

}
