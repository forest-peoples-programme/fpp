<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\TermPartner
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_term_partners",
 *   source_provider = "taxonomy"
 * )
 */
class TermPartner extends FppTerm {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    // Default 2 images limit, can be overridden.
    $this->imageLimit = 10;
  }


  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);

    // select n.nid from nodequeue_subqueue s inner join nodequeue_nodes n
    // on n.sqid = s.sqid where s.qid = 9 and s.reference = 714
    // One 264 has more than one valid node.

    $query = $this->select('nodequeue_subqueue', 's');
    $query->join('nodequeue_nodes', 'n', 'n.sqid = s.sqid');
    $query->condition('s.qid', 9);
    $query->condition('s.reference', $row->getSourceProperty('tid'));
    $query->fields('n', ['nid']);
    $nid = $query->execute()->fetchField();

    if ($nid) {
      // select * from node n inner join node_revisions r on n.nid = r.nid and
      // n.vid = r.vid where n.nid = 5476;
      // Only the title and the body were displayed.
      $query = $this->select('node', 'n');
      $query->join('node_revisions', 'r', 'n.nid = r.nid and n.vid = r.vid');
      $query->condition('n.nid', $nid);
      $query->fields('r', ['title', 'body']);
      $query->fields('n', ['language']);
      $node = $query->execute()->fetchAll();

      if (!empty($node[0])) {
        $row->setSourceProperty('description', '<h2>' . $node[0]['title'] . '</h2>' . $node[0]['body']);
        $row->setSourceProperty('language', $node[0]['language']);
      }
    }

    // Seperate images into logo / other.
    $images = $row->getSourceProperty('topic_images');
    $image = reset($images);
    $logo = NULL;
    do {
      $query = $this->select('term_node', 'tn');
      $query->condition('tn.nid', $image['target_id']);
      $query->condition('tn.tid', 410);
      $query = $query->countQuery();
      $islogo = $query->execute()->fetchField();
      if ($islogo) {
        $logo = $image['target_id'];
        unset($images[key($images)]);
      }
    } while (empty($logo) && ($image = next($images)));
    $row->setSourceProperty('partner_logo', $logo);
    $row->setSourceProperty('topic_images', $images);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
