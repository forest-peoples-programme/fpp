<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_term_topic",
 *   source_provider = "taxonomy"
 * )
 */
class TermTopic extends FppTerm {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
