<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\file\Plugin\migrate\source\d6\File as D6File;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_file",
 * )
 */
class File extends D6File {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Image query.
    $query = $this->select('image', 'i');
    $query->condition('i.fid', $row->getSourceProperty('fid'));
    $query->fields('i', ['image_size']);
    $image = $query->execute()->fetchAll();
    if (count($image)) {
      $image = current($image);
      if ($image['image_size'] != '_original') {
        // Only import originals not derivaties.
        return FALSE;
      }
      // Set filename, from _original, to actual name.
      $row->setSourceProperty('filename', $row->getSourceProperty('origname'));
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
