<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_video",
 *   source_provider = "node"
 * )
 */
class NodeVideo extends FppNode {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    $video = $row->getSourceProperty('field_emvideo');
    $video = current($video);
    if (!empty($video['value'])) {
      if ($video['provider'] == 'youtube') {
        $row->setSourceProperty('video_url', 'https://www.youtube.com/watch?v=' . $video['value']);
      }
      elseif ($video['provider'] == 'vimeo') {
        $row->setSourceProperty('video_url', 'https://vimeo.com/' . $video['value']);
      }
    }
    if (!empty($video['data']['emthumb']['fid'])) {
      $row->setSourceProperty('thumbnail_fid', $data['emthumb']['fid']);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
