<?php

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;
use Drupal\Component\Utility\Unicode;

/**
 * Taxonomy term i18n_strings translations from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_filefield",
 * )
 */
class FileField extends DrupalSqlBase {

  /**
   * Transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager, TransliterationInterface $transliteration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    $this->transliteration = $transliteration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity.manager'),
      $container->get('transliteration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('node', 'n');
    $query->join('content_field_file', 'c', 'c.nid = n.nid and c.vid = n.vid');
    $query->addField('c', 'field_file_fid');
    $query->addField('c', 'field_file_data');
    $query->isNotNull('c.field_file_fid');
    $query->where('n.tnid = 0 OR n.tnid = n.nid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = array(
      'name' => $this->t('The translation of the name.'),
      'language' => $this->t('The language code of the translation'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $data = unserialize($row->getSourceProperty('field_file_data'));
    $language = 'und';
    $description = $this->transliteration->transliterate($data['description']);
    if (stripos($description, 'english') !== FALSE || stripos($description, 'ingles') !== FALSE) {
      $language = 'en';
    }
    if (stripos($description, 'french') !== FALSE || stripos($description, 'francais') !== FALSE) {
      $language = 'fr';
    }
    if (stripos($description, 'spanish') !== FALSE || stripos($description, 'espanol') !== FALSE) {
      $language = 'es';
    }
    if (stripos($description, 'bahasa') !== FALSE) {
      $language = 'id';
    }
    if (stripos($description, 'portuguese') !== FALSE || stripos($description, 'portugese') !== FALSE || stripos($description, 'portugues') !== FALSE) {
      $language = 'pt-br';
    }

    $query = $this->select('files', 'f');
    $query->join('content_field_file', 'c', 'c.field_file_fid = f.fid');
    $query->join('node', 'n', 'c.nid = n.nid');
    $query->isNotNull('c.field_file_fid');
    $query->fields('f', ['fid', 'uid', 'filename', 'filemime', 'status', 'timestamp']);
    $query->fields('c', ['field_file_data']);
    $query->fields('n', ['nid', 'tnid', 'type', 'language', 'title', 'changed']);
    $query->condition('f.fid', $row->getSourceProperty('field_file_fid'));
    $files = $query->execute()->fetchAll(\PDO::FETCH_CLASS);
    $language_match = FALSE;
    $original_language = NULL;
    foreach ($files as $file) {
      if ($file->language == $language) {
        $language_match = TRUE;
        break;
      }
      if ($file->nid == $file->tnid || $file->tnid == 0) {
        $original_language = $file;
        if ($language == 'und') {
          $language_match = TRUE;
          break;
        }
      }
    }
    if (!$language_match && $original_language) {
      $file = $original_language;
    }

    $combined_title = '';
    $title = $this->transliteration->transliterate($file->title);
    if (empty($title)) {
      $combined_title = $data['description'];
    }
    elseif (empty($data['description'])) {
      $combined_title = $file->title;
    }
    elseif (stripos($title, $description) !== FALSE) {
      $combined_title = $file->title;
    }
    elseif (stripos($description, $title) !== FALSE) {
      $combined_title = $data['description'];
    }
    else {
      $combined_title = trim($file->title) . ' - ' . trim($data['description']);
      if (Unicode::strlen($combined_title) > 255) {
        $combined_title = Unicode::truncate(trim($file->title), 253 - Unicode::strlen(trim($data['description'])), TRUE, TRUE) . ' ' . trim($data['description']);
      }
    }

    $row->setSourceProperty('title', $combined_title);
    $row->setSourceProperty('language', $language);
    $row->setSourceProperty('fid', $file->fid);
    $row->setSourceProperty('filename', $file->filename);
    $row->setSourceProperty('file_uid', $file->uid);
    $row->setSourceProperty('filemime', $file->filemime);
    $row->setSourceProperty('status', $file->status);
    $row->setSourceProperty('created', $file->timestamp);
    $row->setSourceProperty('changed', $file->changed);
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['field_file_fid']['type'] = 'integer';
    return $ids;
  }

}
