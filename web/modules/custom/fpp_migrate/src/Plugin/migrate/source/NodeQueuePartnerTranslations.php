<?php

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Taxonomy term i18n_strings translations from database.
 *
 * @MigrateSource(
 *   id = "fpp_nodequeue_partner_translations",
 * )
 */
class NodeQueuePartnerTranslations extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // partners_background: qid = 9

    // select ns.reference as tid, nr.title, nr.body, n.language from
    // nodequeue_nodes nn inner join nodequeue_subqueue ns on nn.sqid = ns.sqid
    // inner join node n on n.tnid = nn.nid inner join node_revisions
    // nr on n.nid = nr.nid and n.vid = nr.vid where nn.qid = 9 and nn.position
    // = 1 and (n.tnid <> 0 AND n.tnid <> n.nid)

    $query = $this->select('nodequeue_nodes', 'nn');
    $query->join('nodequeue_subqueue', 'ns', 'nn.sqid = ns.sqid');
    $query->join('node', 'n', 'n.tnid = nn.nid');
    $query->join('node_revisions', 'nr', 'n.nid = nr.nid and n.vid = nr.vid');
    $query->condition('nn.qid', 9);
    $query->condition('nn.position', 1);
    $query->where('n.tnid <> 0 AND n.tnid <> n.nid');
    $query->addField('ns', 'reference');
    $query->addField('n', 'language');
    $query->fields('nr', ['title', 'body']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = array(
      'tid' => $this->t('The term ID.'),
      'name' => $this->t('The translation of the name.'),
      'description' => $this->t('The translation of the description.'),
      'language' => $this->t('The language code of the translation'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    $row->setSourceProperty('tid', $row->getSourceProperty('reference'));
    $row->setSourceProperty('description', '<h2>' . $row->getSourceProperty('title') . '</h2>' . $row->getSourceProperty('body'));

    // Add title if it's also translated.
    $query = $this->select('term_data', 'td');
    $query->join('i18n_strings', 'i', 'i.objectid = td.tid');
    $query->join('locales_target', 't', 't.lid = i.lid');
    $query->condition('td.tid', $row->getSourceProperty('tid'))
      ->condition('i.property', 'name')
      ->condition('t.language', $row->getSourceProperty('language'))
      ->addField('t', 'translation', 'description');
    $name = $query->execute()->fetchField();
    if (empty($name)) {
      $query = $this->select('term_data', 'td');
      $query->condition('td.tid', $row->getSourceProperty('tid'));
      $query->addField('td', 'name');
      $name = $query->execute()->fetchField();
    }
    $row->setSourceProperty('name', $name);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['reference']['type'] = 'integer';
    $ids['reference']['alias'] = 'ns';
    $ids['language']['type'] = 'string';
    $ids['language']['alias'] = 'n';
    return $ids;
  }

}
