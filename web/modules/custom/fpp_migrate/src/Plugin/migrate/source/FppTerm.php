<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\TermRegion
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\Term;

/**
 * Drupal 6 Term with image from nodequeue.
 */
class FppTerm extends Term {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager);
    // Default 2 images limit, can be overridden.
    $this->imageLimit = 2;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    $row->setSourceProperty('topic_images', $this->retrieveImages($row->getSourceProperty('tid')));
    $row->setSourceProperty('topic_links', $this->retrieveLinks($row->getSourceProperty('tid')));
    return $result;
  }

  /**
   * Get link node data.
   */
  public function retrieveLinks($tid) {
    /*
     * MariaDB [fpp_d6]> select l.field_related_links_url as url, l.field_related_links_title as title from term_node t left join node n on t.nid = n.nid and t.vid = n.vid left join content_field_related_links l on n.nid = l.nid and n.vid = l.vid where n.type = 'link' and t.tid = 724 order by l.delta asc\G
     */
    $query = $this->select('term_node', 't');
    $query->leftJoin('node', 'n', 't.nid = n.nid and t.vid = n.vid');
    $query->leftJoin('content_field_related_links', 'l', 'n.nid = l.nid and n.vid = l.vid');
    $query->condition('n.type', 'link');
    $query->condition('t.tid', $tid);
    $query->fields('l', ['field_related_links_url', 'field_related_links_title']);
    $query->orderBy('l.delta');
    $result = $query->execute()->fetchAll();

    $links = [];
    foreach ($result as $link) {
      // Couple of links miss scheme, couple are internal, rest are http.
      if (substr($link['field_related_links_url'], 0, 4) != 'http') {
        if (substr($link['field_related_links_url'], 0, 3) == 'www') {
          $link['field_related_links_url'] = 'http://' . $link['field_related_links_url'];
        }
        else {
          $link['field_related_links_url'] = 'internal:/' . $link['field_related_links_url'];
        }
      }
      $links[] = [
        'uri' => $link['field_related_links_url'],
        'options' => ['attributes' => []],
        'title' => $link['field_related_links_title'],
      ];
    }

    return $links;
  }

  /**
   * Get subqueue images for a specific term.
   */
  public function retrieveImages($tid) {
    /*
     * +-----+--------------------+
     * | vid | name               |
     * +-----+--------------------+
     * |   1 | Topics             |
     * |   2 | Region             |
     * |  12 | Guiding principles |
     * |  11 | Partners           |
     * |  13 | Tags               |
     * |  14 | Image type         |
     * |  16 | Special blogs      |
     * |  15 | ENewsletters       |
     * +-----+--------------------+
     *
     * +-----+---------------------+
     * | qid | title               |
     * +-----+---------------------+
     * |   5 | location_gallery    |
     * |   6 | topic_gallery       |
     * |  10 | partners_gallery    |
     * |  11 | principles_gallery  |
     * |  14 | programme_gallery   |
     * |  17 | tag_gallery         |
     * |  18 | enewsletter_gallery |
     * +-----+---------------------+
     */
    $qid_lookup = [
      1 => 6,
      2 => 5,
      12 => 11,
      11 => 10,
      13 => 17,
      15 => 18,
    ];
    $qids = [];
    foreach ($this->configuration['vocabulary'] as $vid) {
      $qids[] = $qid_lookup[$vid];
    }
    if (empty($qids)) {
      return [];
    }
    // select n.nid from nodequeue_nodes n inner join nodequeue_subqueue s on
    // s.sqid = n.sqid where s.qid = 5 and s.reference=366 order by n.position
    // asc;
    //
    // s.qid = 5 # queue 5 is Location images.
    // n.position # The first two were shown on the term.
    // Could do this with join but meh.
    $query = $this->select('nodequeue_nodes', 'n');
    $query->join('nodequeue_subqueue', 's', 'n.sqid = s.sqid');
    $query->condition('s.qid', $qids, 'IN');
    $query->condition('s.reference', $tid);
    $query->fields('n', ['nid']);
    $query->orderBy('n.position');
    $result = $query->execute()->fetchAll();

    $images = [];
    $limit = $this->imageLimit;
    while ($limit && (list(, $image) = each($result))) {
      $images[]['target_id'] = $image['nid'];
      $limit--;
    }

    return $images;
  }

}
