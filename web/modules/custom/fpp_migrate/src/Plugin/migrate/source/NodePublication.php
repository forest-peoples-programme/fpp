<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_publications",
 *   source_provider = "node"
 * )
 */
class NodePublication extends FppNode {

  /**
   * {@inheritdoc}
   */
	public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);

    // This can get a lot more clever if it wants.
    $authors = $row->getSourceProperty('field_author');
    $new_authors = [];
    foreach ((array) $authors as $author) {
      $this_authors = explode(',', $author['value']);
      $new_authors = array_merge($new_authors, $this_authors);
    }
    $authors = [];
    foreach ($new_authors as $author) {
      $authors[]['value'] = $author;
    }
    $row->setSourceProperty('field_author', $authors);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
