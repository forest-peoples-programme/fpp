<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_newsletter",
 *   source_provider = "node"
 * )
 */
class NodeNewsletter extends FppNode {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    // Add the single NAT term.
    $query->join('nat', 'nat', 'nat.nid = n.tnid');
    $query->addField('nat', 'tid', 'nat_tid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
	public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);

    // select nn.nid, nn.position from nodequeue_subqueue ns left join
    // nodequeue_nodes nn on nn.sqid = ns.sqid where ns.reference = 676 and
    // ns.qid = 15 order by nn.position;
    $tid = $row->getSourceProperty('nat_tid');
    $query = $this->select('nodequeue_subqueue', 'ns');
    $query->join('nodequeue_nodes', 'nn', 'nn.sqid = ns.sqid');
    $query->condition('ns.reference', $tid);
    $query->condition('ns.qid', 15);
    $query->orderBy('nn.position');
    $query->fields('nn', ['nid', 'position']);
    $nids = $query->execute()->fetchAll();
    $row->setSourceProperty('related', $nids);

    // Images similar to FppTerm
    $tid = $row->getSourceProperty('nat_tid');
    $query = $this->select('nodequeue_subqueue', 'ns');
    $query->join('nodequeue_nodes', 'nn', 'nn.sqid = ns.sqid');
    $query->condition('ns.reference', $tid);
    $query->condition('ns.qid', 18);
    $query->orderBy('nn.position');
    $query->fields('nn', ['nid', 'position']);
    $nids = $query->execute()->fetchAll();
    $row->setSourceProperty('images', $nids);

    // Todo Publication date
    $query = $this->select('term_data', 'td');
    $query->condition('td.tid', $tid);
    $query->fields('td', ['name']);
    $term_name = $query->execute()->fetchField();
    $matches = [];
    preg_match('/\w+\s+\w+$/', $term_name, $matches);
    $timestamp = strtotime('1 '. $matches[0]);
    //print "{$matches[0]} == $timestamp == " . date('l dS \o\f F Y h:i:s A', $timestamp) . "\n";
    $row->setSourceProperty('newsletter_date', date('Y-m-d', $timestamp));

    // Include _term_ path alias.
    $query = $this->select('url_alias', 'ua')
      ->fields('ua', ['dst']);
    $query->condition('ua.src', 'taxonomy/term/' . $tid);
    $alias = $query->execute()->fetchField();
    if (!empty($alias)) {
      $row->setSourceProperty('alias', '/' . $alias);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
