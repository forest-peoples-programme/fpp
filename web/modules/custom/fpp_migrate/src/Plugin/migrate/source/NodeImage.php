<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_image",
 *   source_provider = "node"
 * )
 */
class NodeImage extends FppNode {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->join('image', 'i', "i.nid = n.nid AND i.image_size = '_original'");
    $query->addField('i', 'fid', 'image_fid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);
    // Could do this with join but meh.
    $query = $this->select('term_node', 't');
    $query->join('term_data', 'd', 'd.tid = t.tid AND d.vid = 14');
    $query->condition('t.vid', $row->getSourceProperty('vid'));
    $query->condition('t.nid', $row->getSourceProperty('nid'));
    $query->fields('t', ['tid']);
    $terms = $query->execute()->fetchAll();
    if (count($terms)) {
      $image_type_lookup = [
        '408' => 'illustration',
        '409' => 'photo',
        '410' => 'logo',
        '411' => 'cover',
      ];
      $row->setSourceProperty('image_type', $image_type_lookup[$terms[0]['tid']]);
    }

    // Flatten the alt field to string.
    $alt = $row->getSourceProperty('field_alt');
    if (count($alt)) {
      $row->setSourceProperty('alt_text', $alt[0]['value']);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
