<?php

/**
 * @file
 * Contains \Drupal\fpp_migrate\Plugin\migrate\source\NodeNews
 */

namespace Drupal\fpp_migrate\Plugin\migrate\source;

use Drupal\migrate\Row;

/**
 * Drupal 6 node source from database.
 *
 * @MigrateSource(
 *   id = "fpp_migrate_news",
 *   source_provider = "node"
 * )
 */
class NodeNews extends FppNode {

  /**
   * {@inheritdoc}
   */
	public function prepareRow(Row $row) {
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    return $fields;
  }

}
