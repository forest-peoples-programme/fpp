<?php

namespace Drupal\fpp_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "fpp_cck_date"
 * )
 */
class CckDate extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
		$value['value'] = substr($value['value'], 0, strpos($value['value'], 'T'));
    return $value;
  }

}
